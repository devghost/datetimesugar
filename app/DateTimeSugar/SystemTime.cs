namespace DateTimeSugar {
  using System;

  /// <summary>
  /// SystemTime is used throughout DateTimeSugar to make
  /// testing of date and time possible.
  /// </summary>
  public static class SystemTime {
    public static Func<DateTime> NowFunc = () => DateTime.Now;

    public static DateTime Now {
      get { return NowFunc(); }
    }
  }
}