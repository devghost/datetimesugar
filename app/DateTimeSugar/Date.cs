namespace DateTimeSugar {
  using System;

  public static class Date { 
    public static DateTime Today {
      get { return SystemTime.Now.Date; }
    } 
  }
}