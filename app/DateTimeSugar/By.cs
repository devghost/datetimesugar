namespace DateTimeSugar {
  using System;

  public static class By {
    public static DateTime Tomorrow {
      get { return SystemTime.Now.AddDays(1); }
    }
  }
}