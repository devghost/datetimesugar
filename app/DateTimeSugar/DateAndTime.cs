namespace DateTimeSugar {
  using System;

  public static class DateAndTime {
    public static DateTime Now {
      get { return SystemTime.Now; }
    }
  }
}