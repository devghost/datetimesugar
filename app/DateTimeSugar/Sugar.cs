﻿namespace DateTimeSugar {
  using System;
  using System.Globalization;

  public static class Sugar {
    public enum RoundTo {
      Second,
      Minute,
      Hour,
      Day
    }

    public static TimeSpan Week(this int current) {
      if (current <= 0) {
        throw new ArgumentException("week only supports values larger or equal to 0.");
      }
      return Weeks(current);
    }
    public static TimeSpan Weeks(this int current) {
      if (current <= 0) {
        throw new ArgumentException("Weeks only supports values larger or equal to 1.");
      }
      return new TimeSpan(days: current*7, hours: 0, minutes: 0, seconds: 0);
    }

    public static TimeSpan Day(this int current) { return Days(current); }
    public static TimeSpan Days(this int current) { return new TimeSpan(days: current, hours: 0, minutes: 0, seconds: 0); }
    public static TimeSpan Hour(this int current) { return Hours(current); }
    public static TimeSpan Hours(this int current) { return new TimeSpan(days: 0, hours: current, minutes: 0, seconds: 0); }
    public static TimeSpan Minute(this int current) { return Minutes(current); }
    public static TimeSpan Minutes(this int current) { return new TimeSpan(days: 0, hours: 0, minutes: current, seconds: 0); }
    public static TimeSpan Second(this int current) { return Seconds(current); }
    public static TimeSpan Seconds(this int current) { return new TimeSpan(days: 0, hours: 0, minutes: 0, seconds: current); }

    public static DateTime From(this TimeSpan current, DateTime from) { return from - current; }
    public static DateTime FromNow(this TimeSpan current) { return SystemTime.Now + current; }
    public static DateTime After(this TimeSpan current, DateTime datetime) { return datetime + current; }
    public static DateTime AfterNow(this TimeSpan current) { return After(current, SystemTime.Now); }
    public static DateTime Before(this TimeSpan current, DateTime datetime) { return datetime - current; }
    public static DateTime Before(this TimeSpan current) { return Before(current, SystemTime.Now); }
    public static DateTime Ago(this TimeSpan current) { return SystemTime.Now - current; }
    public static TimeSpan And(this TimeSpan current, TimeSpan and) { return current + and; }
    public static DateTime Until(this TimeSpan current, DateTime targetDate) { return targetDate.Subtract(current); }
    public static DateTime Since(this TimeSpan current, DateTime targetDate) { return targetDate.Add(current); }
    public static DateTime At(this DateTime current) { return current; }
    public static DateTime Noon(this DateTime current) { return new DateTime(year: current.Year, month: current.Month, day: current.Day, hour: 12, minute: 0, second: 0); }
    public static DateTime Midnight(this DateTime current) { return new DateTime(year: current.Year, month: current.Month, day: current.Day, hour: 0, minute: 0, second: 0); }

    public static DateTime LastMonth(this DateTime current) { return current.AddMonths(-1); }
    public static DateTime LastYear(this DateTime current) { return current.AddYears(-1); }
    public static DateTime NextMonth(this DateTime current) { return current.AddMonths(1); }
    public static DateTime NextYear(this DateTime current) { return current.AddYears(1); }

    public static DateTime Next(this DateTime current, DayOfWeek dayOfWeek) {
      var offsetDays = dayOfWeek - current.DayOfWeek;

      if (offsetDays <= 0) {
        offsetDays += 7;
      }

      var result = current.AddDays(offsetDays);
      return result;
    }
    public static DateTime First(this DateTime current) { return current.AddDays(1 - current.Day).Date; }
    public static DateTime First(this DateTime current, DayOfWeek dayOfWeek) {
      var first = current.First();
      if (first.DayOfWeek != dayOfWeek) {
        first = first.Next(dayOfWeek);
      }

      return first.Date;
    }

    public static DateTime Last(this DateTime current) {
      var daysInMonth = DateTime.DaysInMonth(current.Year, current.Month);
      return current.First().AddDays(daysInMonth - 1);
    }
    public static DateTime Last(this DateTime current, DayOfWeek dayOfWeek) {
      var last = current.Last();
      return last.AddDays(Math.Abs(dayOfWeek - last.DayOfWeek)*-1);
    }

    public static DateTime ToLocalTime(this DateTime current, TimeZoneInfo targetTimeZone) { return TimeZoneInfo.ConvertTimeFromUtc(current, targetTimeZone); }
    public static DateTime ToUniversalTime(this DateTime current, TimeZoneInfo sourceTimeZone) { return TimeZoneInfo.ConvertTimeToUtc(current, sourceTimeZone); }

    public static DateTime AddBusinessDays(this DateTime current, int days) {
      var sign = Convert.ToDouble(Math.Sign(days));
      var unsignedDays = Math.Sign(days)*days;
      for (var i = 0; i < unsignedDays; i++) {
        do {
          current = current.AddDays(sign);
        } while (current.DayOfWeek == DayOfWeek.Saturday || current.DayOfWeek == DayOfWeek.Sunday);
      }
      return current;
    }
    public static DateTime Round(this DateTime current, RoundTo rt) {
      var dtRounded = new DateTime();

      switch (rt) {
        case RoundTo.Second:
          dtRounded = new DateTime(current.Year, current.Month, current.Day, current.Hour, current.Minute,
                                   current.Second, current.Kind);
          if (current.Millisecond >= 500) {
            dtRounded = dtRounded.AddSeconds(1);
          }
          break;
        case RoundTo.Minute:
          dtRounded = new DateTime(current.Year, current.Month, current.Day, current.Hour, current.Minute, 0, current.Kind);
          if (current.Second >= 30) {
            dtRounded = dtRounded.AddMinutes(1);
          }
          break;
        case RoundTo.Hour:
          dtRounded = new DateTime(current.Year, current.Month, current.Day, current.Hour, 0, 0, current.Kind);
          if (current.Minute >= 30) {
            dtRounded = dtRounded.AddHours(1);
          }
          break;
        case RoundTo.Day:
          dtRounded = new DateTime(current.Year, current.Month, current.Day, 0, 0, 0, current.Kind);
          if (current.Hour >= 12) {
            dtRounded = dtRounded.AddDays(1);
          }
          break;
      }

      return dtRounded;
    }
    public static string OrdinalSuffix(this DateTime datetime) {
      var day = datetime.Day;

      if (day%100 >= 11 && day%100 <= 13) {
        return String.Concat(day, "th");
      }

      switch (day%10) {
        case 1:
          return String.Concat(day, "st");
        case 2:
          return String.Concat(day, "nd");
        case 3:
          return String.Concat(day, "rd");
        default:
          return String.Concat(day, "th");
      }
    }

    /// <summary>
    ///   Ref. http://dotnetslackers.com/articles/aspnet/5-Helpful-DateTime-Extension-Methods.aspx#s5-tostring-for-nullable-datetime-values
    /// </summary>
    /// <param name="date"> </param>
    /// <returns> </returns>
    public static string ToString(this DateTime? date) { return date.ToString(null, DateTimeFormatInfo.CurrentInfo); }
    public static string ToString(this DateTime? date, string format) { return date.ToString(format, DateTimeFormatInfo.CurrentInfo); }
    public static string ToString(this DateTime? date, IFormatProvider provider) { return date.ToString(null, provider); }
    public static string ToString(this DateTime? date, string format, IFormatProvider provider) { return date.HasValue ? date.Value.ToString(format, provider) : string.Empty; }
  }
}
