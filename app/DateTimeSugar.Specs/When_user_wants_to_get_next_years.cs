﻿namespace DateTimeSugar.Specs {
  using System;
  using DateTimeSugar.Specs.Helpers;
  using Machine.Specifications;

  public class When_user_wants_to_get_next_years : DateTimeIsSystemUnderTest {
    Establish context = () => { SystemTime.NowFunc = () => new DateTime(year: 2010, month: 11, day: 3); };
    Because of = () => { Sut = SystemTime.Now.NextYear(); };

    It should_return_a_datetime_for_next_year = () => Sut.ShouldEqual(SystemTime.Now.AddYears(1));
  }
}