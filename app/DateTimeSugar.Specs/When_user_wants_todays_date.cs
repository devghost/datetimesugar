﻿namespace DateTimeSugar.Specs {
  using System;
  using DateTimeSugar.Specs.Helpers;
  using Machine.Specifications;

  public class When_user_wants_todays_date : DateTimeIsSystemUnderTest {
    Establish context =()=> { SystemTime.NowFunc = () => new DateTime(2011, 04, 29, 14, 18, 23); };
    Because of =()=> { Sut = Date.Today; };
    
    It should = () => Sut.ShouldEqual(new DateTime(year: 2011, month: 4, day: 29));
  }
}