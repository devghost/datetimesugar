﻿namespace DateTimeSugar.Specs {
  using System;
  using DateTimeSugar.Specs.Helpers;
  using Machine.Specifications;

  public class When_user_wants_a_date_1_week_from_now : DateTimeIsSystemUnderTest {
    Establish context = () => { SystemTime.NowFunc = () => new DateTime(2011, 05, 29); };

    private Because of = () => { Sut = 1.Week().FromNow(); };

    private It should_ = () => { Sut.ShouldEqual(new DateTime(2011, 6, 5)); };

  }
}