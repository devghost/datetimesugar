﻿namespace DateTimeSugar.Specs {
  using System;
  using DateTimeSugar.Specs.Helpers;
  using Machine.Specifications;

  public class When_user_wants_a_date_3_business_days_from_now : DateTimeIsSystemUnderTest {
    Establish context = () => { SystemTime.NowFunc = () => new DateTime(2012, 1, 5); };

    private Because of = () => { Sut = SystemTime.Now.AddBusinessDays(3); };

    private It should_ = () => Sut.ShouldEqual(new DateTime(2012, 1, 10));
  }
}