﻿namespace DateTimeSugar.Specs {
  using System;
  using DateTimeSugar.Specs.Helpers;
  using Machine.Specifications;

  public class When_user_wants_a_date_four_hours_ago : DateTimeIsSystemUnderTest {
    Establish context =()=> { SystemTime.NowFunc = () => new DateTime(2011, 04, 29, 14, 18, 23); };
    Because of =()=> { Sut = 4.Hours().Ago(); };
    
    It should = () => Sut.ShouldEqual(SystemTime.Now.AddHours(-4));
  }
}