﻿namespace DateTimeSugar.Specs {
  using System;
  using DateTimeSugar.Specs.Helpers;
  using Machine.Specifications;

  public class When_user_wants_the_date_for_tomorrow_at_noon : DateTimeIsSystemUnderTest {
    Establish context =()=> { SystemTime.NowFunc = () => new DateTime(2011, 04, 29, 14, 18, 23); };
    Because of =()=> { Sut = By.Tomorrow.Noon(); };
    
    It should = () => Sut.ShouldEqual(new DateTime(2011, 04, 30, 12, 0, 0));     
  }
}