﻿namespace DateTimeSugar.Specs {
  using System;
  using DateTimeSugar.Specs.Helpers;
  using Machine.Specifications;

  public class When_user_wants_to_specify_timespan_in_minutes_and_seconds : TimeSpanIsSystemUnderTest {
    Establish context =()=> { };
    Because of =()=> { Sut = 10.Minutes().And(3.Seconds()); };
    
    It should = () => Sut.ShouldEqual(new TimeSpan(hours: 0, minutes: 10, seconds: 3));
  }
}