﻿namespace DateTimeSugar.Specs {
  using System;
  using DateTimeSugar.Specs.Helpers;
  using Machine.Specifications;

  public class When_user_want_a_date_and_time_20_minutes_from_now : DateTimeIsSystemUnderTest {
    Establish context = () => { SystemTime.NowFunc = ()=> new DateTime(year: 2010, month: 10, day: 3, hour: 12, minute: 22, second: 22);  };
    Because of = () => { Sut = 20.Minutes().FromNow(); };

    It should_create_a_datetime_20_minutes_from_now = () => Sut.ShouldEqual(SystemTime.Now.AddMinutes(20));
  }
}