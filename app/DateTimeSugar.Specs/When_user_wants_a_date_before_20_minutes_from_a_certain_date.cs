﻿namespace DateTimeSugar.Specs {
  using System;
  using DateTimeSugar.Specs.Helpers;
  using Machine.Specifications;

  public class When_user_wants_a_date_before_20_minutes_from_a_certain_date : DateTimeIsSystemUnderTest {
    Establish context =()=> { SystemTime.NowFunc = () => new DateTime(2011, 04, 29, 14, 18, 23); };
    Because of =()=> { Sut = 20.Minutes().Before(SystemTime.Now); };
    
    It should = () => Sut.ShouldEqual(SystemTime.Now.AddMinutes(-20));
  }
}