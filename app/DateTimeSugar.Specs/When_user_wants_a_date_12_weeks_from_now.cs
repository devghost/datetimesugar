﻿namespace DateTimeSugar.Specs {
  using System;
  using DateTimeSugar.Specs.Helpers;
  using Machine.Specifications;

  public class When_user_wants_a_date_12_weeks_from_now : DateTimeIsSystemUnderTest {
    private Establish context = () => { SystemTime.NowFunc = () => new DateTime(2011, 05, 29); };
    private Because of = () => { Sut = 12.Weeks().FromNow(); };

    private It should_return_a_date_and_time_7_weeks_from_now = () => Sut.ShouldEqual(new DateTime(
                                                                                        year: 2011,
                                                                                        month: 8,
                                                                                        day: 21
                                                                                        ));
  }
}
