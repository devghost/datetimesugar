﻿namespace DateTimeSugar.Specs {
  using System;
  using DateTimeSugar.Specs.Helpers;
  using Machine.Specifications;

  public class When_user_want_an_ordinal_suffix_for_the_first : StringIsSystemUnderTest {
    Establish context = () => { SystemTime.NowFunc=()=>new DateTime(2012,11,1 ); };

    private Because of = () => { Sut = SystemTime.Now.OrdinalSuffix(); };

    private It should_ = () => Sut.ShouldEqual("1st");

  }
}