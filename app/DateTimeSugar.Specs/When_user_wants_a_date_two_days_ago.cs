﻿namespace DateTimeSugar.Specs {
  using System;
  using DateTimeSugar.Specs.Helpers;
  using Machine.Specifications;

  public class When_user_wants_a_date_two_days_ago : DateTimeIsSystemUnderTest {
    Establish context =()=> { SystemTime.NowFunc = () => new DateTime(2011, 04, 29, 14, 18, 23); };
    Because of =()=> { Sut = 2.Days().Ago(); };
    
    It should = () => Sut.ShouldEqual(SystemTime.Now.AddDays(-2));
  }
}