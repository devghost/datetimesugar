DateTimeSugar adds some syntactic sugar to System.DateTime
==========================================================

Working with System.DateTime is a gruesome experience, and not all that intuitive.
DateTimeSugar aims to provide some readable extensions that makes it easier to work
with dates and time in .NET.

Examples
--------

DateTimeSugar lets your write code like this...

    var backThen = 30.Minutes().Ago();


The code
--------
Take a look at the specs in the code to see what DateTimeSugar is capable of.


Status
------
These little helpers are currently in very much flux and will probably change
and break a lot in the near vicinity, until the api is a tad more stable.

References
----------
DateTimeSugar would not be what it is if it wasn't for a lot of peoples effort.
Here's links to people's useful tidbits that I have in some way included into this library.  
  
Ordinal Suffix DateTime Extension Method by William Duffy  
http://www.wduffy.co.uk/blog/ordinal-suffix-datetime-extension-method/
  
DateTime.Round Extension Method by Mike In Madison  
http://mikeinmadison.wordpress.com/2008/03/12/datetimeround/  
  
AddBusinessDay DateTime Extension Method by Martin Cook
http://www.codeproject.com/Articles/19963/AddBusinessDay-DateTime-Extension-Method  

Helpful DateTime extension methods for dealing with Time Zones by Joshua Flanagan
http://lostechies.com/joshuaflanagan/2011/02/04/helpful-datetime-extension-methods-for-dealing-with-time-zones/